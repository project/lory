<?php

namespace Drupal\lory\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Lory configuration entity.
 *
 * @ConfigEntityType(
 *   id = "lory",
 *   label = @Translation("Lory optionset"),
 *   list_path = "admin/config/media/lory",
 *   config_prefix = "optionset",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "group",
 *     "label",
 *     "optimized",
 *     "status",
 *     "weight",
 *     "options",
 *   }
 * )
 */
class Lory extends ConfigEntityBase implements LoryInterface {

  /**
   * The legacy CTools ID for the configurable optionset.
   *
   * @var string
   */
  protected $name;

  /**
   * The human-readable name for the optionset.
   *
   * @var string
   */
  protected $label;

  /**
   * The optionset group for easy selections.
   *
   * @var string
   */
  protected $group = '';

  /**
   * The weight to re-arrange the order of gridstack optionsets.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The flag indicating to optimize the stored options by removing defaults.
   *
   * @var bool
   */
  protected $optimized = FALSE;

  /**
   * The plugin instance options.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The blazy HTML ID.
   *
   * @var int
   */
  private static $loryId;

  /**
   * Overrides Drupal\Core\Entity\Entity::id().
   */
  public function id() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return $this->group;
  }

  /**
   * {@inheritdoc}
   */
  public function optimized() {
    return $this->optimized;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    // With the Optimized options, all defaults are cleaned out, merge em.
    return array_merge(self::defaultSettings(), $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($name) {
    return isset($this->getOptions()[$name]) ? $this->getOptions()[$name] : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOption($name, $value) {
    $this->options[$name] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options = [], $merged = TRUE) {
    $this->options = $merged ? array_merge($this->getOptions(), $options) : $options;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return self::load('default')->options;
  }

  /**
   * Parse and extract infinite value.
   *
   * @param array $settings
   *   The settings being modified.
   */
  public static function parseResponsive(array &$settings = []) {
    $infinite = &$settings['infinite'];
    if (!is_string($infinite)) {
      return;
    }

    if (strpos($infinite, ':') !== FALSE) {
      $json = [];
      $dimensions = (array) $infinite;
      if (strpos($infinite, ',') !== FALSE) {
        $dimensions = explode(",", $infinite);
      }

      foreach ($dimensions as $dimension) {
        list($window, $total) = array_pad(array_map('trim', explode(":", $dimension, 2)), 2, NULL);
        $json[$window] = (int) $total;
      }

      $settings['responsive'] = (object) $json;
      $infinite = end($json);
    }
    elseif (is_numeric($infinite)) {
      $infinite = (int) $infinite;
    }
  }

  /**
   * Get the current layout.
   *
   * Lory is CSS based slider, a layout controls the correct displays.
   *
   * @param array $settings
   *   The settings being modified.
   */
  public static function getLayout(array &$settings = []) {
    $settings['width']     = isset($settings['width']) ? $settings['width'] : '';
    $settings['infinite']  = $grid = empty($settings['infinite']) ? FALSE : $settings['infinite'];
    $settings['grid']      = $grid;
    $settings['fullwidth'] = $grid == 1 || empty($grid) || (strpos($settings['width'], 'x') !== FALSE);
    $settings['_layout']   = '';

    if (empty($settings['width']) && empty($grid)) {
      $settings['fullwidth'] = TRUE;
    }

    // Variable width wins over the rest of layouts.
    if (!empty($settings['autoWidth'])) {
      $settings['fullwidth'] = $settings['grid'] = FALSE;
      $settings['_layout']   = 'autowidth';
    }

    // Else pass the expected layout based on options.
    if (empty($settings['_layout'])) {
      if ($settings['fullwidth']) {
        $settings['_layout'] = 'fullwidth';
        $settings['grid'] = FALSE;
      }
      elseif ($grid && $grid > 1) {
        $settings['_layout'] = 'grid';
      }
    }
  }

  /**
   * Strip out options containing default values so to have real clean JSON.
   */
  public static function removeDefaultValues(array $js) {
    $settings = array_diff_assoc($js, self::defaultSettings());

    if (!empty($settings['infinite'])) {
      self::parseResponsive($settings);
    }
    else {
      unset($settings['infinite']);
    }

    return $settings;
  }

  /**
   * Load the optionset with a fallback.
   */
  public static function loadWithFallback($id) {
    $optionset = self::load($id);

    // Ensures deleted optionset while being used doesn't screw up.
    if (empty($optionset)) {
      $optionset = self::load('default');
    }
    return $optionset;
  }

}
