<?php

namespace Drupal\lory\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining Lory entity.
 */
interface LoryInterface extends ConfigEntityInterface {

  /**
   * Returns the Lory options by group, or property.
   *
   * @return array
   *   Available options.
   */
  public function getOptions();

  /**
   * Returns the value of a lory setting.
   *
   * @param string $option_name
   *   The option name.
   *
   * @return mixed
   *   The option value.
   */
  public function getOption($option_name);

}
