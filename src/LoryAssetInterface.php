<?php

namespace Drupal\lory;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface defining Lory assets.
 */
interface LoryAssetInterface extends PluginInspectionInterface {

  /**
   * Returns the Lory skins.
   *
   * This can be used to register skins for the Lory. Skins will be
   * available when configuring the Optionset, Field formatter, or Views style,
   * or custom coded lorys.
   *
   * Lory skins get a unique CSS class to use for styling, e.g.:
   * If your skin name is "my_module_lory_carousel_rounded", the CSS class is:
   * lory--skin--my-module-lory-carousel-rounded
   *
   * A skin can specify CSS and JS files to include when Lory is displayed,
   * except for a thumbnail skin which accepts CSS only.
   *
   * Each skin supports 5 keys:
   * - name: The human readable name of the skin.
   * - description: The description about the skin, for help and manage pages.
   * - css: An array of CSS files to attach.
   * - js: An array of JS files to attach, e.g.: image zoomer, reflection, etc.
   * - group: A string grouping the current skin: main, thumbnail.
   * - provider: A module name registering the skins.
   *
   * @return array
   *   The array of the main and thumbnail skins.
   */
  public function skins();

}
