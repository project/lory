<?php

namespace Drupal\lory\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a LoryAsset item annotation object.
 *
 * @Annotation
 */
class LoryAsset extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
