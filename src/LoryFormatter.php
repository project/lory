<?php

namespace Drupal\lory;

use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\blazy\BlazyFormatter;

/**
 * Implements LoryFormatterInterface.
 */
class LoryFormatter extends BlazyFormatter implements LoryFormatterInterface {

  /**
   * {@inheritdoc}
   */
  public function buildSettings(array &$build, $items) {
    $settings = &$build['settings'];

    // Prepare integration with Blazy.
    $settings['item_id']   = 'slide';
    $settings['namespace'] = 'lory';
    $settings['nav']       = empty($settings['navset']) ? FALSE : $settings['navset'];

    // Pass basic info to parent::buildSettings().
    parent::buildSettings($build, $items);
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbnail(array $settings = [], $item = NULL) {
    if (!empty($settings['uri'])) {
      return [
        '#theme'      => 'image_style',
        '#style_name' => empty($settings['thumbnail_style']) ? 'thumbnail' : $settings['thumbnail_style'],
        '#uri'        => $settings['uri'],
        '#item'       => $item,
        '#alt'        => $item && $item instanceof ImageItem ? $item->getValue()['alt'] : '',
      ];
    }
    return [];
  }

}
