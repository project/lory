<?php

namespace Drupal\lory;

use Drupal\Component\Plugin\Mapper\MapperInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages lory assets.
 */
class LoryAssetManager extends DefaultPluginManager implements LoryAssetManagerInterface, MapperInterface {

  /**
   * The app root.
   *
   * @var \SplString
   */
  protected $root;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Static cache for the skin definition.
   *
   * @var array
   */
  protected $skinDefinition;

  /**
   * Static cache for the skins by group.
   *
   * @var array
   */
  protected $skinsByGroup;

  /**
   * The library info definition.
   *
   * @var array
   */
  protected $libraryInfoBuild;

  /**
   * The supported skins.
   *
   * @var array
   */
  private static $skins = [
    'feature' => 'features',
    'navskin' => 'skins',
    'skin'    => 'skins',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, $root, ConfigFactoryInterface $config) {
    parent::__construct('Plugin/lory', $namespaces, $module_handler, LoryAssetInterface::class, 'Drupal\lory\Annotation\LoryAsset');

    $this->root = $root;
    $this->config = $config;

    $this->alterInfo('lory_asset_info');
    $this->setCacheBackend($cache_backend, 'lory_asset_plugins');
  }

  /**
   * Returns app root.
   */
  public function root() {
    return $this->root;
  }

  /**
   * Returns lory config shortcut.
   */
  public function config($key = '', $settings = 'lory.settings') {
    return $this->config->get($settings)->get($key);
  }

  /**
   * Returns cache backend service.
   */
  public function getCache() {
    return $this->cacheBackend;
  }

  /**
   * Returns the supported skins.
   */
  public static function getConstantAssets() {
    return self::$skins;
  }

  /**
   * {@inheritdoc}
   */
  public function load($plugin_id) {
    return $this->createInstance($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple() {
    $assets = [];
    foreach ($this->getDefinitions() as $definition) {
      array_push($assets, $this->createInstance($definition['id']));
    }
    return $assets;
  }

  /**
   * Returns lory skins registered via SlickSkin plugin and or defaults.
   */
  public function getAssets() {
    if (!isset($this->skinDefinition)) {
      $cid = 'lory_asset_data';

      if ($cache = $this->cacheBackend->get($cid)) {
        $this->skinDefinition = $cache->data;
      }
      else {
        $methods = ['skins', 'features'];
        $skins = $items = [];
        foreach ($this->loadMultiple() as $skin) {
          foreach ($methods as $method) {
            $items[$method] = $skin->{$method}();
          }
          $skins = NestedArray::mergeDeep($skins, $items);
        }

        $count = count($skins);
        $tags = Cache::buildTags($cid, ['count:' . $count]);
        $this->cacheBackend->set($cid, $skins, Cache::PERMANENT, $tags);

        $this->skinDefinition = $skins;
      }
    }
    return $this->skinDefinition;
  }

  /**
   * Returns available skins for select options.
   */
  public function getSkinAsOptions() {
    if (!isset($this->skinOptions)) {
      $this->skinOptions = $this->getAssetsByGroup('skins', TRUE);
    }
    return $this->skinOptions;
  }

  /**
   * Returns available lory skins by group.
   */
  public function getAssetsByGroup($group = 'skins', $use_as_option = FALSE) {
    if (!isset($this->skinsByGroup[$group])) {
      $skins = [];
      foreach ($this->getAssets()[$group] as $skin => $properties) {
        $item = $use_as_option ? strip_tags($properties['name']) : $properties;
        $skins[$skin] = $item;
      }
      $this->skinsByGroup[$group] = $skins;
    }
    return $this->skinsByGroup[$group];
  }

  /**
   * {@inheritdoc}
   */
  public function attach(array &$load, array $attach = []) {
    $attach += ['skin' => FALSE, 'width' => ''];

    $load['library'][] = 'lory/load';

    foreach (self::getConstantAssets() as $group => $method) {
      if ($group == 'feature') {
        continue;
      }

      // @todo: Supports multiple transitions like random transitions.
      $skin = isset($attach[$group]) ? $attach[$group] : '';
      if (!empty($skin)) {
        $skins = $this->getAssets()[$method];
        $provider = isset($skins[$skin]['provider']) ? $skins[$skin]['provider'] : 'lory';

        $load['library'][] = 'lory/skin';
        $load['library'][] = 'lory/' . $provider . '.' . $group . '.' . $skin;
      }
    }

    foreach ($this->getAssets()['features'] as $key => $properties) {
      $value = isset($properties['name']) ? $properties['name'] : $key;
      $group = 'feature';
      if (!empty($attach[$value])) {
        $provider = isset($properties['provider']) ? $properties['provider'] : 'lory';
        $load['library'][] = 'lory/' . $provider . '.' . $group . '.' . $key;
      }
    }

    if (isset($attach['_layout']) && $attach['_layout'] == 'grid') {
      $load['library'][] = 'lory/grid';
    }

    // Attach default JS settings.
    $settings = LoryDefault::jsFixedSettings();
    $settings['infinite'] = FALSE;
    $load['drupalSettings']['lory'] = $settings;
  }

  /**
   * Implements hook_library_info_build().
   */
  public function libraryInfoBuild() {
    if (!isset($this->libraryInfoBuild)) {
      $libraries = [];
      foreach (self::getConstantAssets() as $group => $method) {
        if ($skins = $this->getAssets()[$method]) {
          foreach ($skins as $key => $skin) {
            $provider = isset($skin['provider']) ? $skin['provider'] : 'lory';
            $id = $provider . '.' . $group . '.' . $key;

            foreach (['css', 'js', 'dependencies'] as $property) {
              if (isset($skin[$property]) && is_array($skin[$property])) {
                $libraries[$id][$property] = $skin[$property];
              }
            }
            $libraries[$id]['dependencies'][] = 'lory/skin';
          }
        }
      }

      $this->libraryInfoBuild = $libraries;
    }

    return $this->libraryInfoBuild;
  }

}
