<?php

namespace Drupal\lory;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\blazy\BlazyManagerInterface;

/**
 * Defines re-usable services and functions for Lory plugins.
 */
interface LoryManagerInterface extends BlazyManagerInterface, TrustedCallbackInterface {}
