<?php

namespace Drupal\lory;

use Drupal\blazy\BlazyDefault;

/**
 * Defines default options.
 */
class LoryDefault extends BlazyDefault {

  /**
   * Returns Blazy specific breakpoints.
   */
  public static function getConstantBreakpoints() {
    return ['xs', 'md', 'lg'];
  }

  /**
   * Returns default classes.
   */
  public static function jsOptionsetSettings() {
    $options = [
      'adaptiveHeight'    => FALSE,
      'autoWidth'         => FALSE,
      'ease'              => 'ease',
      'enableMouseEvents' => FALSE,
      'focusOnSelect'     => FALSE,
      'infinite'          => '',
      'randomize'         => FALSE,
      'slideSpeed'        => 300,
      'slidesToScroll'    => 1,
      'snapBackSpeed'     => 200,
      'initialSlide'      => 0,
      'rewind'            => FALSE,
      'vertical'          => FALSE,
      'width'             => '',
    ] + self::features();

    ksort($options);
    return $options;
  }

  /**
   * Returns fixed JavaScript settings which may not be available at forms.
   *
   * They are excluded from UI as otherwise breaking the module, unless extra
   * logic provided to cover various use cases, avoided in the first place.
   * The rest are trivial options with little value, excluded to simplify UI.
   */
  public static function jsFixedSettings() {
    return [
      'classNameFrame'          => 'lory__frame',
      'classNameSlideContainer' => 'lory__track',
      'classNamePrevCtrl'       => 'lory__prev',
      'classNameNextCtrl'       => 'lory__next',
      'classNameActiveSlide'    => 'is-current',
      'rewindOnResize'          => FALSE,
      'rewindSpeed'             => 600,
    ] + self::jsOptionsetSettings();
  }

  /**
   * Returns non-ui excluded settings.
   */
  public static function excludedSettings() {
    return [
      '_detached' => TRUE,
      'blazy' => TRUE,
      'lazy' => TRUE,
    ] + self::jsFixedSettings();
  }

  /**
   * Returns the supported features for options.
   *
   * @param bool $hide
   *   The flag whether to hide, or show the options.
   */
  public static function features($hide = TRUE) {
    $features = lory('asset')->getAssets()['features'];
    $options  = [];

    foreach ($features as $key => $feature) {
      $hidden = isset($feature['type']) && $feature['type'] == 'hidden';
      if ($hidden && $hide) {
        continue;
      }

      $option = isset($feature['name']) ? $feature['name'] : $key;
      $options[$option] = isset($feature['default']) ? $feature['default'] : '';
    }

    return $options;
  }

  /**
   * Returns default base settings.
   */
  public static function navSettings() {
    return [
      'navset'            => '',
      'navpos'            => '',
      'navskin'           => '',
      'thumbnail_caption' => '',
      'thumbnail_style'   => '',
    ];
  }

  /**
   * Returns default base settings.
   */
  public static function baseSettings() {
    return ['optionset' => 'default', 'view_mode' => ''] + self::navSettings() + parent::baseSettings();
  }

  /**
   * Returns image-related field formatter and Views settings.
   */
  public static function imageSettings() {
    return self::baseSettings() + parent::baseImageSettings();
  }

  /**
   * Returns the views style plugin settings.
   */
  public static function viewsSettings() {
    return self::baseSettings() + parent::viewsSettings();
  }

  /**
   * Returns fieldable entity formatter and Views settings.
   */
  public static function baseEntitySettings() {
    return ['thumbnail' => ''] + self::baseSettings();
  }

  /**
   * Returns fieldable entity formatter and Views settings.
   */
  public static function entitySettings() {
    return self::baseEntitySettings() + self::imageSettings() + self::viewsSettings() + parent::entitySettings();
  }

  /**
   * Returns fieldable entity formatter and Views settings.
   */
  public static function extendedSettings() {
    return self::jsFixedSettings() + self::entitySettings() + parent::extendedSettings();
  }

  /**
   * Returns HTML or layout related settings to shut up notices.
   */
  public static function htmlSettings() {
    return [
      'asNavFor'     => '',
      'display'      => 'main',
      'id'           => '',
      'navpos'       => FALSE,
      'skin'         => '',
      'width'        => '',
      'media_switch' => '',
      'optionset'    => 'default',
      'view_name'    => '',
    ];
  }

  /**
   * Returns lory theme properties.
   */
  public static function themeProperties() {
    return [
      'attributes',
      'items',
      'options',
      'optionset',
      'settings',
    ];
  }

}
