<?php

namespace Drupal\lory\Form;

use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\Html;
use Drupal\blazy\Form\BlazyAdminInterface;
use Drupal\lory\LoryManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides admin form specific to Blazy admin formatter.
 */
class LoryAdmin implements LoryAdminInterface {

  use StringTranslationTrait;

  /**
   * The blazy admin service.
   *
   * @var \Drupal\blazy\Form\BlazyAdminInterface
   */
  protected $blazyAdmin;

  /**
   * The lory manager service.
   *
   * @var \Drupal\lory\LoryManagerInterface
   */
  protected $manager;

  /**
   * Constructs a LoryAdmin object.
   *
   * @param \Drupal\blazy\Form\BlazyAdminInterface $blazy_admin
   *   The blazy admin service.
   * @param \Drupal\lory\LoryManagerInterface $manager
   *   The lory manager service.
   */
  public function __construct(BlazyAdminInterface $blazy_admin, LoryManagerInterface $manager) {
    $this->blazyAdmin = $blazy_admin;
    $this->manager    = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('blazy.admin.extended'), $container->get('lory.manager'));
  }

  /**
   * Returns the blazy admin formatter.
   */
  public function blazyAdmin() {
    return $this->blazyAdmin;
  }

  /**
   * Returns the slick manager.
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * Defines re-usable form elements.
   */
  public function buildSettingsForm(array &$form, $definition = []) {
    $definition['namespace']        = 'lory';
    $definition['skins']            = $this->getSkinAsOptions('skins', 'main');
    $definition['style']            = FALSE;
    $definition['grid_form']        = FALSE;
    $definition['thumbnail_style']  = isset($definition['thumbnail_style']) ? $definition['thumbnail_style'] : FALSE;
    $definition['optionsets']       = isset($definition['optionsets']) ? $definition['optionsets'] : $this->getOptionsetsByGroupOptions('main');
    $definition['responsive_image'] = isset($definition['responsive_image']) ? $definition['responsive_image'] : TRUE;

    foreach (['optionsets', 'skins'] as $key) {
      if (isset($definition[$key]['default'])) {
        ksort($definition[$key]);
        $definition[$key] = ['default' => $definition[$key]['default']] + $definition[$key];
      }
    }

    $this->openingForm($form, $definition);
    $this->mainForm($form, $definition);
    $this->closingForm($form, $definition);
  }

  /**
   * Returns the opening form elements.
   */
  public function openingForm(array &$form, $definition = []) {
    $path   = drupal_get_path('module', 'lory');
    $readme = Url::fromUri('base:' . $path . '/README.md')->toString();

    $this->blazyAdmin->openingForm($form, $definition);

    if (isset($form['optionset']) && $this->manager()->getModuleHandler()->moduleExists('lory_ui')) {
      $route_name = 'entity.lory.collection';
      $form['optionset']['#description'] = $this->t('Manage optionsets at <a href=":url" target="_blank">the optionset admin page</a>.', [':url' => Url::fromRoute($route_name)->toString()]);
    }

    if (isset($form['skin'])) {
      $form['skin']['#description'] = $this->t('Skins allow various layouts with just CSS. Some options below depend on a skin. Leave empty to DIY. To register skins, see <a href=":url">README</a> for relevant info.', [':url' => $readme]);
    }
  }

  /**
   * Returns the main form elements.
   */
  public function mainForm(array &$form, $definition = []) {
    $path   = drupal_get_path('module', 'lory');
    $readme = Url::fromUri('base:' . $path . '/README.md')->toString();

    if (!empty($definition['image_style_form'])) {
      $definition['thumbnail_style'] = isset($definition['thumbnail_style']) ? $definition['thumbnail_style'] : TRUE;
      $this->blazyAdmin->imageStyleForm($form, $definition);
    }

    if (!empty($definition['media_switch_form'])) {
      $definition['ratios'] = isset($definition['ratios']) ? $definition['ratios'] : TRUE;
      $this->blazyAdmin->mediaSwitchForm($form, $definition);
    }

    if (!empty($definition['fieldable_form'])) {
      $this->blazyAdmin->fieldableForm($form, $definition);
    }

    if (!empty($definition['nav']) && !empty($definition['optionsets'])) {
      $optionsets = $this->getOptionsetsByGroupOptions('nav');
      ksort($optionsets);

      $form['navset'] = [
        '#type'        => 'select',
        '#title'       => $this->t('Nav optionset'),
        '#options'     => $optionsets,
        '#description' => $this->t('If provided, asnavfor aka thumbnail navigation applies. Leave empty to not use thumbnail navigation.'),
        '#weight'      => -108,
      ];

      $form['navskin'] = [
        '#type'        => 'select',
        '#title'       => $this->t('Nav skin'),
        '#options'     => $this->getSkinAsOptions('skins', 'nav'),
        '#description' => $this->t('Thumbnail navigation skin. See main <a href="@url" target="_blank">README</a> for details on Skins. Leave empty to not use thumbnail navigation.', ['@url' => $readme]),
        '#weight'      => -107,
      ];

      if (!empty($definition['navpos'])) {
        if (isset($form['image_style'])) {
          $form['image_style']['#weight'] = -105;
        }
        if (isset($form['view_mode'])) {
          $form['view_mode']['#weight'] = -105;
        }
        if (isset($form['thumbnail_style'])) {
          $form['thumbnail_style']['#weight'] = -99;
        }
        $form['navpos'] = [
          '#type'         => 'select',
          '#title'        => $this->t('Nav position'),
          '#empty_option' => $this->t('Bottom'),
          '#weight'       => -104,
          '#description'  => $this->t('By default thumbnail is positioned at bottom. Hence to change its position. Only reasonable with 1 visible main stage at a time. Further theming is required as usual. Overlay is absolutely positioned over the stage rather than sharing the space. Except any TOP/BOTTOM, the rest requires Vertical option enabled for Optionset nav.'),
          '#options' => [
            'top'         => $this->t('Top'),
            'over-bottom' => $this->t('Overlay bottom'),
            'over-top'    => $this->t('Overlay top'),
          ],
          '#states' => [
            'visible' => [
              'select[name*="[navset]"]' => ['!value' => ''],
            ],
          ],
        ];
      }

      if (isset($definition['thumb_captions'])) {
        $form['thumbnail_caption'] = [
          '#type'        => 'select',
          '#title'       => $this->t('Nav caption'),
          '#options'     => is_array($definition['thumb_captions']) ? $definition['thumb_captions'] : [],
          '#description' => $this->t('Thumbnail caption maybe just title/ plain text. If Thumbnail image style is not provided, the thumbnail pagers will be just text like regular tabs.'),
          '#states' => [
            'visible' => [
              'select[name*="[navset]"]' => ['!value' => ''],
            ],
          ],
          '#weight'      => -103,
        ];
      }

      if (!empty($definition['thumbnails'])) {
        $form['thumbnail']['#title'] = $this->t('Nav thumbnail');
        $form['thumbnail']['#description'] = $this->t("Only needed if <em>Navset</em> is provided. Leave empty to not use thumbnail pager.");

        // If not views, this is entity reference providing nav as view modes.
        if (empty($definition['id']) && !empty($definition['target_type'])) {
          $form['thumbnail']['#title'] = $this->t('Nav view mode');
          $form['thumbnail']['#weight'] = -90;
          $form['thumbnail']['#description'] .= ' ' . $this->t('Manage view modes on the <a href=":view_modes">View modes page</a>.', [':view_modes' => Url::fromRoute('entity.entity_view_mode.collection')->toString()]);
        }
      }
    }

    if (!empty($definition['breakpoints'])) {
      $this->blazyAdmin->breakpointsForm($form, $definition);
    }
  }

  /**
   * Returns the closing ending form elements.
   */
  public function closingForm(array &$form, $definition = []) {
    $this->blazyAdmin->closingForm($form, $definition);
  }

  /**
   * Returns available skins for select options.
   */
  public function getSkinAsOptions($method = 'skins', $group = '') {
    $skins = [];
    foreach ($this->manager->assetManager()->getAssets()[$method] as $skin => $properties) {
      $existing_group = isset($properties['group']) ? $properties['group'] : '';
      $skin_name = isset($properties['name']) ? Html::escape($properties['name']) : $skin;

      if (!empty($group)) {
        if ($existing_group != $group) {
          continue;
        }
        $skins[$skin] = $skin_name;
      }
      else {
        $skins[$skin] = $skin_name;
      }
    }

    return $skins;
  }

  /**
   * Returns available lory optionsets by group.
   */
  public function getOptionsetsByGroupOptions($group = '') {
    $optionsets = $groups = $ungroups = [];
    $lories = $this->manager->entityLoadMultiple('lory');
    foreach ($lories as $lory) {
      $name = Html::escape($lory->label());
      $id = $lory->id();
      $current_group = $lory->getGroup();
      if (!empty($group)) {
        if ($current_group) {
          if ($current_group != $group) {
            continue;
          }
          $groups[$id] = $name;
        }
        else {
          $ungroups[$id] = $name;
        }
      }
      $optionsets[$id] = $name;
    }

    return $group ? array_merge($ungroups, $groups) : $optionsets;
  }

  /**
   * Return the field formatter settings summary.
   */
  public function getSettingsSummary($definition = [], $plugin = NULL) {
    return $this->blazyAdmin->getSettingsSummary($definition);
  }

  /**
   * Returns available fields for select options.
   */
  public function getFieldOptions($target_bundles = [], $allowed_field_types = [], $entity_type_id = 'media', $target_type = '') {
    return $this->blazyAdmin->getFieldOptions($target_bundles, $allowed_field_types, $entity_type_id, $target_type);
  }

  /**
   * Returns re-usable logic, styling and assets across fields and Views.
   */
  public function finalizeForm(array &$form, $definition = []) {
    return $this->blazyAdmin->finalizeForm($form, $definition);
  }

}
