<?php

namespace Drupal\lory\Plugin\lory;

use Drupal\lory\LoryAssetBase;

/**
 * Provides lory assets.
 *
 * @LoryAsset(
 *   id = "lory_asset",
 *   label = @Translation("Lory asset")
 * )
 */
class LoryAsset extends LoryAssetBase {

  /**
   * Sets the lory skins.
   *
   * @inheritdoc
   */
  protected function setSkins() {
    // If you copy this file, be sure to add base_path() before any asset path
    // (css or js) as otherwise failing to load the assets. Your module can
    // register paths pointing to a theme. Check out lory.api.php for details.
    $path = base_path() . drupal_get_path('module', 'lory');

    $slicebox = [
      'd3Plane'         => TRUE,
      'orientation'     => 'r',
      'cuboidsCount'    => 7,
      'maxCuboidsCount' => 7,
      'cuboidsRandom'   => TRUE,
      'disperseFactor'  => 30,
      'perspective'     => 1300,
      'shadow'          => TRUE,
    ];

    $skins = [
      'default' => [
        'name' => 'Default',
        'css' => [
          'theme' => [
            $path . '/css/theme/lory.theme--default.css' => [],
          ],
        ],
      ],
      'fade' => [
        'name' => 'Fade: Fader',
        'description' => $this->t('One slide visible at a time. Best with Infinite 1, autoplay, use e.g. optionset Fade.'),
        'css' => [
          'theme' => [
            $path . '/css/theme/lory.theme--fade.css' => [],
          ],
        ],
        'dependencies' => ['lory/extend', 'lory/fade'],
        'options' => [
          'fade' => TRUE,
          'fps' => .2,
          'animation' => TRUE,
          'effect' => ['fade'],
        ],
      ],
      'kenburnsin' => [
        'name' => 'Fade: Ken Burns in',
        'description' => $this->t('One slide visible at a time. Best with Infinite 1, autoplay, use e.g. optionset Fade.'),
        'css' => [
          'theme' => [
            $path . '/css/theme/lory.theme--kenburnsin.css' => [],
          ],
        ],
        'dependencies' => ['lory/extend', 'lory/fade'],
        'options' => [
          'fade' => TRUE,
          'fps' => .2,
          'animation' => TRUE,
          'effect' => ['fade', 'kenburnsin'],
        ],
      ],
      'kenburnsout' => [
        'name' => 'Fade: Ken Burns out',
        'description' => $this->t('One slide visible at a time. Best with Infinite 1, autoplay, use e.g. optionset Fade.'),
        'css' => [
          'theme' => [
            $path . '/css/theme/lory.theme--kenburnsout.css' => [],
          ],
        ],
        'dependencies' => ['lory/extend', 'lory/fade'],
        'options' => [
          'fade' => TRUE,
          'fps' => .2,
          'animation' => TRUE,
          'effect' => ['fade', 'kenburnsout'],
        ],
      ],
      'slicebox' => [
        'name' => '3d: Slicebox',
        'description' => $this->t('One slide visible at a time. Best with uniform images, use e.g. optionset Slicebox. Requires initialSlide 1.'),
        'css' => [
          'theme' => [
            $path . '/css/theme/lory.theme--slicebox.css' => [],
          ],
        ],
        'dependencies' => ['lory/extend', 'lory/d3', 'lory/slicebox'],
        'options' => $slicebox + [
          'effect' => ['d3', 'slicebox'],
        ],
      ],
      'reel' => [
        'name' => 'Reel',
        'description' => $this->t('Multiple slides visible at once. Best with Infinite > 1.'),
        'css' => [
          'theme' => [
            'css/theme/lory.theme--nav.css' => [],
            'css/theme/lory.theme--reel.css' => [],
          ],
        ],
      ],
      'zoomin' => [
        'name' => 'Zoom in',
        'description' => $this->t('Multiple slides visible at once. Best with Infinite > 1, and non-vertical.'),
        'css' => [
          'theme' => [
            'css/theme/lory.theme--nav.css' => [],
            'css/theme/lory.theme--zoomin.css' => [],
          ],
        ],
      ],
      'fullscreen' => [
        'name' => 'Full screen',
        'description' => $this->t('One slide visible at a time. Best with Infinite 1.'),
        'css' => [
          'theme' => [
            'css/theme/lory.theme--fullscreen.css' => [],
          ],
        ],
      ],
      'fullwidth' => [
        'name' => 'Full width',
        'description' => $this->t('One slide visible at a time. Best with Infinite 1, to put caption overlay over the main stage.'),
        'css' => [
          'theme' => [
            'css/theme/lory.theme--fullwidth.css' => [],
          ],
        ],
      ],
      'split' => [
        'name' => 'Split',
        'description' => $this->t('Image and caption are displayed side by side.'),
        'css' => [
          'theme' => [
            'css/theme/lory.theme--split.css' => [],
          ],
        ],
      ],
    ];

    foreach ($skins as $key => $skin) {
      $nav = in_array($key, ['reel', 'zoomin']);
      $skins[$key]['group'] = $nav ? 'nav' : 'main';
      $skins[$key]['provider'] = 'lory';
    }

    return $skins;
  }

  /**
   * Provides optional features.
   *
   * Features are extensions which define extra behaviors for the plugin.
   * They may be hidden from UI as needed, or exposed as options.
   *
   * Unless hidden, e.g.: a WIP, or OTF calc, they are available at Lory UI.
   *
   * The provided assets will be registered as libraries automatically, and only
   * loaded when relevant features are enabled, or detected for use.
   *
   * Each feature supports a few keys:
   * - name: The human readable name of the feature used for JS key.
   * - description: The feature description for form item description.
   * - default: Default value to use for the UI option, and typecasting.
   * - css: An array of CSS files to attach.
   * - js: An array of extension JS files to attach.
   * - dependencies: any valid library name to depend on.
   * - provider: A module name registering the features.
   * - type: The form element type. Use `hidden` to hide it at UI.
   */
  protected function setFeatures() {
    $features = [
      'arrows' => [
        'name' => 'arrows',
        'js' => [
          'js/features/lory.feature.arrows.min.js' => [],
        ],
        'default' => TRUE,
        'dependencies' => ['lory/extend', 'lory/load'],
        'description' => $this->t('Use arrows. Uncheck to disable.'),
      ],
      'arrowdown' => [
        'name' => 'arrowDown',
        'css' => [
          'component' => [
            'css/components/lory.feature.arrowdown.css' => [],
          ],
        ],
        'default' => '',
        'dependencies' => ['lory/extend', 'lory/load'],
        'description' => $this->t('Valid CSS selector to scroll to, e.g.: #main, or #content. Leave empty to not use arrow down. Note! This requires Jumper.module.'),
      ],
      'asnavfor' => [
        'name' => 'asNavFor',
        'js' => [
          'js/features/lory.feature.asnavfor.min.js' => [],
        ],
        'default' => '',
        'dependencies' => ['lory/extend', 'lory/load'],
        'type' => 'hidden',
        'description' => $this->t('Note! Intentionally hidden.'),
      ],
      'autoplay' => [
        'name' => 'autoplay',
        'js' => [
          'js/features/lory.feature.autoplay.min.js' => [],
        ],
        'default' => FALSE,
        'dependencies' => ['lory/extend', 'lory/load'],
        'description' => $this->t('Enable autoplay.'),
      ],
      'centermode' => [
        'name' => 'centerMode',
        'js' => [
          'js/features/lory.feature.centermode.min.js' => [],
        ],
        'default' => FALSE,
        'dependencies' => ['lory/extend', 'lory/load'],
        'description' => $this->t('Enables centered view. Only use with odd number of infinite option > 1. Ignored if fade is enabled. Leave it disabled if 1 slide is visible at a time.'),
      ],
      'dots' => [
        'name' => 'dots',
        'js' => [
          'js/features/lory.feature.dots.min.js' => [],
        ],
        'default' => FALSE,
        'dependencies' => ['lory/extend', 'lory/load'],
        'description' => $this->t('Use dots. Uncheck to disable.'),
      ],
      'lazy' => [
        'name' => 'lazy',
        'js' => [
          'js/features/lory.feature.lazy.min.js' => [],
        ],
        'default' => 'blazy',
        'dependencies' => ['blazy/load', 'lory/extend', 'lory/load'],
        'type' => 'hidden',
        'description' => $this->t('Note! Intentionally hidden.'),
      ],
      'responsive' => [
        'name' => 'responsive',
        'js' => [
          'js/features/lory.feature.responsive.min.js' => [],
        ],
        'default' => '',
        'dependencies' => ['lory/extend', 'lory/load', 'lory/grid'],
        'type' => 'hidden',
        'description' => $this->t('Note! Intentionally hidden.'),
      ],
      'vertical' => [
        'name' => 'vertical',
        'css' => [
          'component' => [
            'css/components/lory.feature.vertical.css' => [],
          ],
        ],
        'js' => [
          'js/features/lory.feature.vertical.min.js' => [],
        ],
        'default' => FALSE,
        'dependencies' => ['lory/extend', 'lory/load'],
        'description' => $this->t('WIP!'),
      ],
    ];

    foreach ($features as $key => $feature) {
      $features[$key]['provider'] = 'lory';
    }

    if (function_exists('jumper_help')) {
      $features['arrowdown']['dependencies'][] = 'jumper/load';
    }

    return $features;
  }

}
