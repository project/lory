<?php

namespace Drupal\lory\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\blazy\Dejavu\BlazyStylePluginBase;
use Drupal\lory\LoryDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Lory style plugin.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "lory",
 *   title = @Translation("Lory"),
 *   help = @Translation("Display the results in a Lory."),
 *   theme = "lory_wrapper",
 *   register_theme = FALSE,
 *   display_types = {"normal"}
 * )
 */
class LoryViews extends BlazyStylePluginBase {

  /**
   * The lory service manager.
   *
   * @var \Drupal\lory\LoryManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->manager = $container->get('lory.manager');
    $instance->blazyManager = isset($instance->blazyManager) ? $instance->blazyManager : $container->get('blazy.manager');

    return $instance;
  }

  /**
   * Returns the lory admin.
   */
  public function admin() {
    return \Drupal::service('lory.admin');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = [];
    foreach (LoryDefault::viewsSettings() as $key => $value) {
      $options[$key] = ['default' => $value];
    }
    return $options + parent::defineOptions();
  }

  /**
   * Overrides parent::buildOptionsForm().
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $fields = [
      'captions',
      'classes',
      'images',
      'layouts',
      'links',
      'overlays',
      'thumbnails',
      'thumb_captions',
      'titles',
    ];

    $definition = $this->getDefinedFieldOptions($fields);
    foreach (['fieldable_form', 'id', 'nav', 'navpos', 'vanilla'] as $key) {
      $definition[$key] = TRUE;
    }

    $count = count($definition['captions']);
    $definition['captions_count'] = $count;
    $definition['opening_class'] = 'form--views';
    $definition['_views'] = TRUE;
    $this->admin()->buildSettingsForm($form, $definition);

    $wide = $count > 2 ? ' form--wide form--caption-' . $count : ' form--caption-' . $count;
    $title = '<p class="form__header form__title">';
    $title .= $this->t('Check Vanilla if using content/custom markups, not fields. <small>See it under <strong>Format > Show</strong> section. Otherwise lory markups apply which require some fields added below.</small>');
    $title .= '</p>';

    $form['opening']['#markup'] = '<div class="form--lory form--lory form--views form--half form--vanilla has-tooltip' . $wide . '">' . $title;

    if (isset($form['image'])) {
      $form['image']['#description'] .= ' ' . $this->t('Use Blazy formatter to have it lazyloaded. Other supported Formatters: Colorbox, Intense, Responsive image, Video Embed Field, Youtube Field.');
    }
    if (isset($form['overlay'])) {
      $form['overlay']['#description'] .= ' ' . $this->t('Be sure to CHECK "<strong>Style settings > Use field template</strong>" _only if using Lory formatter for nested sliders, otherwise keep it UNCHECKED!');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildSettings() {
    // @todo move it into self::prepareSettings() post blazy:2.x.
    $this->options['item_id'] = 'slide';
    $this->options['namespace'] = 'lory';
    $settings = parent::buildSettings();

    // Prepare needed settings to work with.
    $settings['caption'] = array_filter($settings['caption']);
    $settings['namespace'] = 'lory';
    $settings['navset'] = !empty($settings['navset']) && isset($this->view->result[1]) ? $settings['navset'] : FALSE;

    return $settings;
  }

  /**
   * Overrides StylePluginBase::render().
   */
  public function render() {
    $settings = $this->buildSettings();
    $item_id  = $settings['item_id'];

    $elements = [];
    foreach ($this->renderGrouping($this->view->result, $settings['grouping']) as $rows) {
      $build = $this->buildElements($settings, $rows);

      // Supports Blazy multi-breakpoint images if using Blazy formatter.
      $settings['first_image'] = isset($rows[0]) ? $this->getFirstImage($rows[0]) : [];

      $build['settings'] = $settings;
      $build['media_attributes']['class'][] = $item_id . '__media';

      $elements = $this->manager->build($build);
      unset($build);
    }
    return $elements;
  }

  /**
   * Returns lory contents.
   */
  public function buildElements(array $settings, $rows) {
    $build   = [];
    $item_id = $settings['item_id'];

    // @todo enable after proper checks.
    // $settings = array_filter($settings);
    foreach ($rows as $index => $row) {
      $this->view->row_index = $index;

      $slide = [];
      $thumb = $slide[$item_id] = [];

      // Provides a potential unique thumbnail different from the main image.
      if (!empty($settings['thumbnail'])) {
        $thumbnail = $this->getFieldRenderable($row, 0, $settings['thumbnail']);
        if (isset($thumbnail['rendered']['#image_style'], $thumbnail['rendered']['#item']) && $item = $thumbnail['rendered']['#item']) {
          $uri = (($entity = $item->entity) && empty($item->uri)) ? $entity->getFileUri() : $item->uri;
          $settings['thumbnail_style'] = $thumbnail['rendered']['#image_style'];
          $settings['thumbnail_uri'] = $this->manager->entityLoad($settings['thumbnail_style'], 'image_style')->buildUri($uri);
        }
      }

      $slide['settings'] = $settings;

      // Use Vanilla lory if so configured, ignoring Lory markups.
      if (!empty($settings['vanilla'])) {
        $slide[$item_id] = $this->view->rowPlugin->render($row);
      }
      else {
        $this->buildElement($slide, $row, $index);

        if (!empty($settings['navset'])) {
          $thumb[$item_id]  = empty($settings['thumbnail']) ? [] : $this->getFieldRendered($index, $settings['thumbnail']);
          $thumb['caption'] = empty($settings['thumbnail_caption']) ? [] : $this->getFieldRendered($index, $settings['thumbnail_caption']);

          $build['thumb']['items'][$index] = $thumb;
        }
      }

      if (!empty($settings['class'])) {
        $classes = $this->getFieldString($row, $settings['class'], $index);
        $slide['settings']['class'] = empty($classes[$index]) ? [] : $classes[$index];
      }

      if (empty($slide[$item_id]) && !empty($settings['image'])) {
        $slide[$item_id] = $this->getFieldRendered($index, $settings['image']);
      }

      $build['items'][$index] = $slide;
      unset($slide);
    }

    unset($this->view->row_index);

    return $build;
  }

}
