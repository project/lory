<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\blazy\Dejavu\BlazyEntityBase;
use Drupal\lory\LoryDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for lory entity reference formatters without field details.
 */
abstract class LoryEntityFormatterBase extends BlazyEntityBase {

  use LoryFormatterViewTrait;
  use LoryFormatterTrait {
    buildSettings as traitBuildSettings;
  }

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return self::injectServices($instance, $container, 'entity');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return LoryDefault::baseEntitySettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entities = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($entities)) {
      return [];
    }

    return $this->commonViewElements($items, $langcode, $entities);
  }

  /**
   * Builds the settings.
   *
   * @todo inherit and extend parent post Blazy 2.x release.
   */
  public function buildSettings() {
    return ['vanilla' => TRUE] + $this->traitBuildSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function getScopedFormElements() {
    return [
      'no_layouts' => TRUE,
      // 'fieldable_form' => TRUE,
      // 'namespace'      => 'lory',
      // 'nav'            => TRUE,
      // 'navpos'         => TRUE,
      // 'thumbnails'     => $this->admin()->blazyAdmin()->getViewModeOptions($definition['target_type']),
    ] + $this->getCommonScopedFormElements() + parent::getScopedFormElements();
  }

}
