<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

use Drupal\blazy\Dejavu\BlazyEntityReferenceBase;
use Drupal\lory\LoryDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for lory entity reference formatters with field details.
 *
 * @see \Drupal\lory_media\Plugin\Field\FieldFormatter
 * @see \Drupal\lory_paragraphs\Plugin\Field\FieldFormatter
 */
abstract class LoryEntityReferenceFormatterBase extends BlazyEntityReferenceBase {

  use LoryFormatterViewTrait;
  use LoryFormatterTrait {
    buildSettings as traitBuildSettings;
  }

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return self::injectServices($instance, $container, 'entity');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return LoryDefault::extendedSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function buildElementThumbnail(array &$build, $element, $entity, $delta) {
    // @todo move it to Lory as too specific for Lory which has thumbnail.
    // The settings in $element has updated metadata extracted from media.
    $settings = $element['settings'];
    $item_id = $settings['item_id'];
    if (!empty($settings['nav'])) {
      // Thumbnail usages: asNavFor pagers, dot, arrows, photobox thumbnails.
      $element[$item_id] = empty($settings['thumbnail_style']) ? [] : $this->formatter->getThumbnail($settings, $element['item']);
      $element['caption'] = empty($settings['thumbnail_caption']) ? [] : $this->blazyEntity->getFieldRenderable($entity, $settings['thumbnail_caption'], $settings['view_mode']);

      $build['thumb']['items'][$delta] = $element;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getScopedFormElements() {
    $admin       = $this->admin();
    $target_type = $this->getFieldSetting('target_type');
    $views_ui    = $this->getFieldSetting('handler') == 'default';
    $bundles     = $views_ui ? [] : $this->getFieldSetting('handler_settings')['target_bundles'];
    $texts       = ['text', 'text_long', 'string', 'string_long', 'link'];
    $texts       = $admin->getFieldOptions($bundles, $texts, $target_type);

    return [
      'thumb_captions' => $texts,
      'navpos'         => TRUE,
      'nav'            => TRUE,
    ] + $this->getCommonScopedFormElements() + parent::getScopedFormElements();
  }

}
