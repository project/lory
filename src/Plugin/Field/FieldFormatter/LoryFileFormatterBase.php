<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\Xss;
use Drupal\blazy\Plugin\Field\FieldFormatter\BlazyFileFormatterBase;
use Drupal\lory\LoryDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for lory image and file ER formatters.
 */
abstract class LoryFileFormatterBase extends BlazyFileFormatterBase {

  use LoryFormatterTrait;
  use LoryFormatterViewTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return self::injectServices($instance, $container, 'image');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return LoryDefault::imageSettings();
  }

  /**
   * {@inheritdoc}
   *
   * @todo use $this->commonViewElements() post blazy:2.x release.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $entities = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($entities)) {
      return [];
    }

    return $this->commonViewElements($items, $langcode, $entities);
  }

  /**
   * Build the lory carousel elements.
   */
  public function buildElements(array &$build, $files) {
    $settings   = $build['settings'];
    $item_id    = $settings['item_id'];
    $tn_caption = empty($settings['thumbnail_caption']) ? NULL : $settings['thumbnail_caption'];

    foreach ($files as $delta => $file) {
      $settings['delta'] = $delta;
      $settings['type']  = 'image';

      /** @var Drupal\image\Plugin\Field\FieldType\ImageItem $item */
      $item = $file->_referringItem;

      $settings['file_tags'] = $file->getCacheTags();
      $settings['uri']       = $file->getFileUri();

      $element = ['item' => $item, 'settings' => $settings];

      // @todo remove, no longer file entity/VEF/M for pure Media.
      $this->buildElement($element, $file);
      $settings = $element['settings'];

      // Build caption if so configured.
      if (!empty($settings['caption'])) {
        foreach ($settings['caption'] as $caption) {
          if ($element['item'] && $caption_content = $element['item']->{$caption}) {
            $element['captions'][$caption] = ['#markup' => Xss::filterAdmin($caption_content)];
          }
        }
      }

      // Image with responsive image, lazyLoad, and lightbox supports.
      $element['media_attributes']['class'][] = $item_id . '__media';
      $element[$item_id] = $this->blazyManager()->getBlazy($element);

      // Build individual lory item.
      $build['items'][$delta] = $element;

      // Build individual lory thumbnail.
      if (!empty($settings['navset'])) {
        // Thumbnail usages: asnavfor pagers, dot, arrows, photobox thumbnails.
        $thumb[$item_id]  = empty($settings['thumbnail_style']) ? [] : $this->formatter->getThumbnail($settings, $element['item']);
        $thumb['caption'] = empty($element['item']->{$tn_caption}) ? [] : ['#markup' => Xss::filterAdmin($element['item']->{$tn_caption})];

        $build['thumb']['items'][$delta] = $thumb;
        unset($thumb);
      }

      unset($element);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getScopedFormElements() {
    return [
      'nav'            => TRUE,
      'navpos'         => TRUE,
      'thumb_captions' => ['title' => $this->t('Title'), 'alt' => $this->t('Alt')],
    ] + $this->getCommonScopedFormElements() + parent::getScopedFormElements();
  }

}
