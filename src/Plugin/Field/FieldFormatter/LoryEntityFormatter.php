<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'Lory Entity Reference' formatter.
 *
 * @FieldFormatter(
 *   id = "lory_entity",
 *   label = @Translation("Lory Vanilla"),
 *   field_types = {"entity_reference", "entity_reference_revisions"}
 * )
 */
class LoryEntityFormatter extends LoryEntityFormatterBase {

  /**
   * Returns item contents.
   */
  public function buildElement(array &$build, $entity, $langcode) {
    parent::buildElement($build, $entity, $langcode);

    $settings = $build['settings'];
    $item_id  = $settings['item_id'];
    $delta    = $settings['delta'];

    // Build individual lory thumbnail from a dedicated view mode.
    if (!empty($settings['navset']) && !empty($settings['thumbnail'])) {
      $thumb[$item_id] = $this->manager()->getEntityTypeManager()->getViewBuilder($entity->getEntityTypeId())->view($entity, $settings['thumbnail'], $langcode);

      $build['thumb']['items'][$delta] = $thumb;
      unset($thumb);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getSetting('target_type') != 'file';
  }

}
