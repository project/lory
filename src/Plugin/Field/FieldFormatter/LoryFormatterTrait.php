<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\lory\LoryDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Trait common for Lory formatters.
 */
trait LoryFormatterTrait {

  /**
   * The lory field formatter manager.
   *
   * @var \Drupal\lory\LoryFormatterInterface
   */
  protected $formatter;

  /**
   * The lory manager.
   *
   * @var \Drupal\lory\LoryManagerInterface
   */
  protected $manager;

  /**
   * Returns the lory field formatter service.
   */
  public function formatter() {
    return $this->formatter;
  }

  /**
   * Returns the lory service.
   */
  public function manager() {
    return $this->manager;
  }

  /**
   * Returns the blazy manager.
   */
  public function blazyManager() {
    return $this->formatter;
  }

  /**
   * Returns the blazy admin service.
   */
  public function admin() {
    return \Drupal::service('lory.admin');
  }

  /**
   * Injects DI services.
   */
  protected static function injectServices($instance, ContainerInterface $container, $type = '') {
    $instance->formatter = $instance->blazyManager = $container->get('lory.formatter');
    $instance->manager = $container->get('lory.manager');

    // Blazy:2.x+ might already set these, provides a failsafe.
    if ($type == 'image' || $type == 'entity') {
      $instance->imageFactory = isset($instance->imageFactory) ? $instance->imageFactory : $container->get('image.factory');
      if ($type == 'entity') {
        $instance->loggerFactory = isset($instance->loggerFactory) ? $instance->loggerFactory : $container->get('logger.factory');
        $instance->blazyEntity = isset($instance->blazyEntity) ? $instance->blazyEntity : $container->get('blazy.entity');
        $instance->blazyOembed = isset($instance->blazyOembed) ? $instance->blazyOembed : $instance->blazyEntity->oembed();
      }
    }

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return $this->admin()->getSettingsSummary($this->getScopedFormElements());
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->isMultiple();
  }

  /**
   * Builds the settings.
   */
  public function buildSettings() {
    $settings = array_merge($this->getCommonFieldDefinition(), $this->getSettings());
    $settings['third_party'] = $this->getThirdPartySettings();
    $settings['item_id'] = 'slide';
    $settings['blazy'] = TRUE;
    $settings['lazy'] = 'blazy';

    return $settings;
  }

  /**
   * Defines the common scope for both front and admin.
   */
  public function getCommonFieldDefinition() {
    $field = $this->fieldDefinition;
    return [
      'namespace'         => 'lory',
      'current_view_mode' => $this->viewMode,
      'field_name'        => $field->getName(),
      'field_type'        => $field->getType(),
      'entity_type'       => $field->getTargetEntityTypeId(),
      'plugin_id'         => $this->getPluginId(),
      'target_type'       => $this->getFieldSetting('target_type'),
      'excludes'          => LoryDefault::excludedSettings(),
    ];
  }

  /**
   * Defines the common scope for the form elements.
   */
  public function getCommonScopedFormElements() {
    return ['settings' => $this->getSettings()] + $this->getCommonFieldDefinition();
  }

}
