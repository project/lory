<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'lory file' formatter.
 *
 * @FieldFormatter(
 *   id = "lory_file",
 *   label = @Translation("Lory File"),
 *   description = @Translation("Display the referenced entities as a Lory carousel."),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 *
 * @todo TBD; deprecate for LoryMediaFormatter.
 */
class LoryFileFormatter extends LoryFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return self::injectServices($instance, $container, 'entity');
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array &$build, $entity) {
    $settings = $build['settings'];
    $data = [];
    /** @var Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $item */
    // EntityReferenceItem provides $item->entity Drupal\file\Entity\File.
    if (empty($build['item'])) {
      // @todo remove condition post blazy:2.x.
      if (method_exists($this->blazyOembed, 'getImageItem')) {
        $data = $this->blazyOembed->getImageItem($entity);
      }

      if ($data) {
        $build['item'] = $data['item'];
        $build['settings'] = array_merge($settings, $data['settings']);
      }
    }

    $this->blazyOembed->getMediaItem($build, $entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getScopedFormElements() {
    return [
      'fieldable_form' => TRUE,
      'multimedia'     => TRUE,
      'view_mode'      => $this->viewMode,
    ] + parent::getScopedFormElements();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();
    return $storage->isMultiple() && $storage->getSetting('target_type') === 'file';
  }

}
