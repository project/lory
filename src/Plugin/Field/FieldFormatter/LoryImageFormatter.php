<?php

namespace Drupal\lory\Plugin\Field\FieldFormatter;

/**
 * Plugin for the Lory image formatter.
 *
 * @FieldFormatter(
 *   id = "lory_image",
 *   label = @Translation("Lory Image"),
 *   field_types = {"image"}
 * )
 */
class LoryImageFormatter extends LoryFileFormatterBase {}
