<?php

namespace Drupal\lory;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base implementation for lory asset plugins.
 */
abstract class LoryAssetBase extends PluginBase implements LoryAssetInterface {

  use StringTranslationTrait;

  /**
   * The features definitions.
   *
   * @var array
   */
  protected $features;

  /**
   * Plugin skins definition.
   *
   * @var array
   */
  protected $skins;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->features = $this->setFeatures();
    $this->skins = $this->setSkins();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->configuration['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function features() {
    return $this->features;
  }

  /**
   * {@inheritdoc}
   */
  public function skins() {
    return $this->skins;
  }

  /**
   * Sets the optional plugin features.
   */
  protected function setFeatures() {
    return [];
  }

  /**
   * Sets the required plugin main/thumbnail skins.
   */
  abstract protected function setSkins();

}
