<?php

/**
 * @file
 * Hooks and preprocess functions for the Lory module.
 */

use Drupal\Core\Template\Attribute;
use Drupal\Component\Serialization\Json;
use Drupal\blazy\Blazy;
use Drupal\lory\Entity\Lory;

/**
 * Prepares variables for lory.html.twig templates.
 */
function template_preprocess_lory(&$variables) {
  $element   = $variables['element'];
  $settings  = isset($element['#settings']) ? $element['#settings'] : [];
  $options   = isset($element['#options']) ? $element['#options'] : [];
  $optionset = isset($element['#optionset']) ? $element['#optionset'] : Lory::loadWithFallback($settings['optionset']);
  $js        = $options ? array_merge($optionset->getOptions(), $options) : $optionset->getOptions();

  // Prepare attributes.
  $settings['id']    = Blazy::getHtmlId('lory', $settings['id']);
  $settings['count'] = count($element['#items']);
  $attributes        = &$variables['attributes'];
  $attributes['id']  = $settings['id'];

  // Disabled controls when lacking of items.
  if ($settings['count'] == 1) {
    $variables['control'] = $js['arrows'] = $js['dots'] = FALSE;
  }
  else {
    $variables['control'] = TRUE;
    if (!empty($settings['asNavFor'])) {
      $js['asNavFor'] = $settings['asNavFor'];
    }
  }

  // Supports Blazy.
  if ($settings['display'] == 'nav') {
    $settings['effect'] = $js['effect'] = '';
  }
  else {
    if (!empty($js['effect']) && is_array($js['effect'])) {
      $settings['effect'] = $js['effect'];
      foreach ($js['effect'] as $effect) {
        $settings['attributes']['class'][] = 'lory--' . $effect;
      }
    }

    Blazy::containerAttributes($attributes, $settings);
  }

  // Make slick language-direction-aware.
  $language = \Drupal::languageManager()->getCurrentLanguage();
  if ($language->getDirection() == 'rtl') {
    $attributes['dir'] = $language->getDirection();
    $js['rtl'] = $language->getDirection() ? TRUE : FALSE;
  }

  // Add the configuration as JSON object into the lory container.
  if ($json = Lory::removeDefaultValues($js)) {
    $settings = array_merge($settings, $json);
    $attributes['data-lory'] = Json::encode($json);
  }

  // Pass settings and attributes to twig.
  $variables['settings'] = $settings;

  // Passes individual item to Twig template.
  $variables['items'] = [];
  foreach ($element['#items'] as $delta => &$item) {
    $settings['delta'] = $delta;
    $variables['items'][$delta] = _lory_slide($item, $settings);
  }
}

/**
 * Prepares variables for individual item.
 */
function _lory_slide(array &$item, array $settings) {
  $content = isset($item['slide']) ? $item['slide'] : [];
  $caption = isset($item['caption']) ? array_filter($item['caption']) : [];
  $item_settings = isset($item['settings']) ? array_merge($settings, $item['settings']) : $settings;
  $item_attributes = isset($item['attributes']) ? $item['attributes'] : [];

  $item_settings['current_item'] = $settings['display'];
  unset($item['settings'], $item['attributes'], $item['item']);

  // Prevents complication with clones, asnavfor, and events.
  $item_attributes['data-lid'] = $settings['delta'];
  if (!empty($settings['class'])) {
    $item_attributes['class'][] = $settings['class'];
  }
  if (!empty($settings['layout'])) {
    $item_attributes['class'][] = 'slide--' . str_replace('_', '-', $settings['layout']);
  }
  // List items support attributes via the '#wrapper_attributes' property.
  if (!empty($item['#wrapper_attributes'])) {
    $item_attributes = $item['#wrapper_attributes'];
  }

  return [
    'slide' => $content,
    'caption' => $caption,
    'attributes' => new Attribute($item_attributes),
    'settings' => $item_settings,
  ];
}

/**
 * Prepares variables for lory-wrapper.html.twig templates.
 */
function template_preprocess_lory_wrapper(&$variables) {
  foreach (['items', 'settings'] as $key) {
    $variables[$key] = isset($variables['element']["#$key"]) ? $variables['element']["#$key"] : [];
  }
}
