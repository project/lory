<?php

namespace Drupal\lory_ui\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\blazy\BlazyGrid;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Lory optionsets.
 */
class LoryListBuilder extends DraggableListBuilder {

  /**
   * The lory manager.
   *
   * @var \Drupal\lory\LoryManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id())
    );

    $instance->manager = $container->get('lory.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lory_list_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'label'        => $this->t('Optionset'),
      'optimized'    => $this->t('Optimized'),
      'infinite'     => $this->t('Infinite'),
      'initialSlide' => $this->t('initialSlide'),
      'slideSpeed'   => $this->t('slideSpeed'),
      'width'        => $this->t('Width'),
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = Html::escape($entity->label());

    $optimized = $entity->optimized() ? $this->t('Yes') : $this->t('No');
    $value = $entity->id() == 'default' ? $this->t('Must not be') : $optimized;
    $row['optimized']['#markup'] = $value;

    foreach (['infinite', 'initialSlide', 'slideSpeed', 'width'] as $key) {
      $value = $entity->getOption($key);

      if (empty($value)) {
        $default = $key == 'width' ? '-' : 0;
        $value = $key == 'infinite' ? 'FALSE' : $default;
      }

      $row[$key]['#markup'] = $value;
    }

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if (isset($operations['edit'])) {
      $operations['edit']['title'] = $this->t('Configure');
    }

    $operations['duplicate'] = [
      'title'  => $this->t('Duplicate'),
      'weight' => 15,
      'url'    => $entity->toUrl('duplicate-form'),
    ];

    if ($entity->id() == 'default') {
      unset($operations['delete'], $operations['edit']);
    }

    return $operations;
  }

  /**
   * Adds some descriptive text to the lory optionsets list.
   *
   * @return array
   *   Renderable array.
   *
   * @see admin/config/development/configuration/single/export
   */
  public function render() {
    $manager = $this->manager;

    $build['description'] = [
      '#markup' => $this->t("<p>Manage the Lory optionsets. Optionsets are Config Entities.</p><p>By default, when this module is enabled, a few optionsets are created from configuration to get up and running quickly. Use the Operations column to edit, clone and delete optionsets.<br><strong>Important!</strong> Avoid overriding Default optionset as it is meant for Default -- checking and cleaning. Use Duplicate instead. Otherwise messes are yours.<br><br>Lory doesn't need Lory UI to run. It is always safe to uninstall Lory UI once done with optionsets.</p>"),
    ];

    $availaible_skins = [];
    $skins = $manager->assetManager()->getAssets()['skins'];

    foreach ($skins as $key => $skin) {
      $name = isset($skin['name']) ? $skin['name'] : $key;
      $group = isset($skin['group']) ? Html::escape($skin['group']) : 'None';
      $provider = isset($skin['provider']) ? Html::escape($skin['provider']) : 'Lory';
      $description = isset($skin['description']) ? Html::escape($skin['description']) : $this->t('No description');

      $markup = '<h3>' . $this->t('@skin <br><small>Id: @id | Group: @group | Provider: @provider</small>', [
        '@skin' => $name,
        '@id' => $key,
        '@group' => $group,
        '@provider' => $provider,
      ]) . '</h3>';

      $markup .= '<p><em>&mdash; ' . $description . '</em></p>';

      $availaible_skins[$key] = [
        '#markup' => '<div class="messages messages--status">' . $markup . '</div>',
      ];
    }

    ksort($availaible_skins);
    $availaible_skins = ['default' => $availaible_skins['default']] + $availaible_skins;

    $settings = [];
    $settings['grid'] = 3;
    $settings['grid_medium'] = 2;
    $settings['blazy'] = FALSE;
    $settings['style'] = 'column';

    $header = '<br><hr><h2>' . $this->t('Available skins') . '</h2>';
    $header .= '<p>' . $this->t('Some skin works best with a specific Optionset, and vice versa. Use matching names if found. Else happy adventure!') . '</p>';
    $build['skins_header']['#markup'] = $header;
    $build['skins_header']['#weight'] = 20;

    $build['skins'] = BlazyGrid::build($availaible_skins, $settings);
    $build['skins']['#weight'] = 21;
    $build['skins']['#attached'] = $manager->attach($settings);
    $build['skins']['#attached']['library'][] = 'blazy/admin';

    $build[] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->messenger()->addMessage($this->t('The optionsets order has been updated.'));
  }

}
