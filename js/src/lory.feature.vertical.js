/**
 * @file
 * @todo: Remove when core library supports vertical.
 */

(function (_db) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Overrides lory.prototype.update() to add vertical feature.
   */
  _l.update = (function (_lory) {
    return function (e) {
      var me = this;
      var ret = _lory.apply(this, arguments);

      if (me.options.vertical === true) {
        if (e.type === 'after.lory.init') {
          me.setVerticalHeight();
        }
        else {
          me.setVerticalPos();
        }
      }

      return ret;
    };
  })(_l.update);

  /**
   * Sets container vertical height.
   */
  _l.setVerticalHeight = function () {
    var me = this;
    var vHeight = me.$slides[0].offsetHeight;

    if (me.$slider.classList.contains('lory--nav')) {
      vHeight = me.$wrap.querySelector('.lory--main').offsetHeight;
    }
    else if (me.options.infinite > 1) {
      vHeight = vHeight * me.options.infinite;
    }

    // Use maxHeight, not height, to support small device as non-vertical.
    me.$list.style.maxHeight = vHeight + 'px';

    me.setVerticalPos();
  };

  /**
   * Sets vertical transform properties.
   */
  _l.setVerticalPos = function () {
    var me = this;
    var targetLeft = 0;
    var $curr = me.$curr;

    if ($curr !== null) {
      targetLeft = $curr.offsetTop * -1;

      if (me.options.centerMode === true) {
        targetLeft += (me.$list.offsetHeight - $curr.offsetHeight) / 2;
      }

      me.setCss(targetLeft);
    }
  };

}(dBlazy));
