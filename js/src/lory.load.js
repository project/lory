/**
 * @file
 * Provides lory loader.
 */

(function (Drupal, drupalSettings, _db, document) {

  'use strict';

  /**
   * Lory utility functions.
   *
   * @param {HTMLElement} slider
   *   The lory slider HTML element.
   */
  function doLory(slider) {
    var data = slider.getAttribute('data-lory');
    var options = !data ? drupalSettings.lory : _db.extend({}, drupalSettings.lory, _db.parse(data));

    // Build lory.
    new lory(slider, options);
    slider.classList.add('lory--on');
  }

  /**
   * Attaches lory behavior to HTML element .lory.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.lory = {
    attach: function (context) {
      var lories = context.querySelectorAll('.lory:not(.lory--on):not(.delory)');
      _db.once(_db.forEach(lories, doLory, context));
    }
  };

}(Drupal, drupalSettings, dBlazy, this.document));
