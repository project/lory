/**
 * @file
 */

(function () {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Set prev/ next disabled attributes on after.lory.init and after.lory.slide.
   *
   * @param {Object.Event} e
   *   The event triggered by various `click` events.
   *
   * @todo: Remove when core provides this.
   */
  _l.updateArrows = function (e) {
    var me = this;
    var $prev = me.$prev;
    var $next = me.$next;
    var o = me.options;

    if (o.arrows && !o.infinite) {
      $prev.removeAttribute('disabled');
      $next.removeAttribute('disabled');

      if (me.currentSlide === me.lastSlide) {
        $next.setAttribute('disabled', 'disabled');
      }
      else if (me.currentSlide === 0) {
        $prev.setAttribute('disabled', 'disabled');
      }
    }
  };

}());
