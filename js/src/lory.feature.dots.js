/**
 * @file
 */

(function (_db) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Overrides lory.prototype.attachEvents() to add dots feature.
   */
  _l.attachEvents = (function (_lory) {
    return function () {
      var me = this;

      me.$dots = me.$slider.querySelector('.lory__dots');

      if (me.count > 1 && me.options.dots === true) {
        var events = {
          'before.lory.init': me.dotBuild,
          'after.lory.init': me.dotInit,
          'after.lory.slide': me.dotChange
        };

        // Add event listeners.
        _db.forEach(events, function (fn, event) {
          me.$slider.addEventListener(event, fn.bind(me), false);
        });
      }

      return _lory.call(this);
    };
  })(_l.attachEvents);

  /**
   * Builds dots pagination.
   *
   * @param {Object.Event} e
   *   The event triggered by a `before.lory.init` event.
   */
  _l.dotBuild = function (e) {
    var me = this;
    var dots = me.$dots;

    if (dots.children.length === 0) {
      var fragment = document.createDocumentFragment();
      for (var i = 0; i < me.count; i++) {
        var btn = document.createElement('li');

        btn.textContent = i;
        fragment.appendChild(btn);
      }

      dots.appendChild(fragment);

      dots.classList.remove('visually-hidden');
    }
  };

  /**
   * Adds dot after.lory.init events.
   *
   * @param {Object.Event} e
   *   The event triggered by a `after.lory.init` event.
   */
  _l.dotInit = function (e) {
    var me = this;
    var dots = me.$dots.childNodes;

    dots[me.currentSlide].classList.add(me.currClass);

    for (var i = 0; i < me.count; i++) {
      dots[i].addEventListener('click', function (e) {
        var curr = Array.prototype.indexOf.call(dots, e.target);
        me.currentSlide = curr;

        if (!e.target.classList.contains(me.currClass)) {
          me.slideTo(curr);
        }
      });
    }
  };

  /**
   * Adds dot after.lory.slide events.
   *
   * @param {Object.Event} e
   *   The event triggered by a `after.lory.slide` event.
   */
  _l.dotChange = function (e) {
    var me = this;
    var dots = me.$dots.childNodes;

    for (var i = 0; i < me.count; i++) {
      dots[i].classList.remove(me.currClass);
    }

    dots[e.detail.currentSlide - 1].classList.add(me.currClass);
  };

}(dBlazy));
