/**
 * @file
 */

(function (Drupal) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Forces loading blazy on certain actions.
   *
   * @param {Object.Event} e
   *   The event triggered by a slide change event.
   */
  _l.doBlazy = function (e) {
    var me = this;
    var lazy = '.b-lazy:not(.b-loaded)';
    var slides = me.$track;
    var blaze = slides.querySelector(lazy);

    if (blaze === null) {
      return;
    }

    if (Drupal.blazy) {
      var elms = slides.querySelectorAll(lazy);

      if (elms !== null) {
        Drupal.blazy.init.load(elms);
      }

      if (slides.querySelector('.media--loading') !== null) {
        Drupal.blazy.init.revalidate();
      }
    }
  };

}(Drupal));
