/**
 * @file
 */

(function (_db) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Overrides lory.prototype.attachEvents() to add autoplay feature.
   */
  _l.attachEvents = (function (_lory) {
    return function () {
      var me = this;

      me.autoId = null;
      me.fpsInterval = null;
      me.now = null;
      me.then = null;
      me.elapsed = null;
      me.hidden = 'hidden';
      me.visibilityChange = 'visibilitychange';
      me.stop = false;

      if (me.options.autoplay && me.count > 1) {
        me.autoPlay();

        document.addEventListener(me.visibilityChange, me.autoChange.bind(this));

        var events = {
          'mouseenter': me.autoChange,
          'mouseleave': me.autoChange,
          'on.lory.touchstart': me.autoChange,
          'on.lory.touchend': me.autoChange
        };

        // Add event listeners.
        _db.forEach(events, function (fn, event) {
          me.$slider.addEventListener(event, fn.bind(me), false);
        });
      }

      return _lory.call(this);
    };
  })(_l.attachEvents);

  /**
   * Executes autoplay if requested.
   */
  _l.autoPlay = function () {
    var me = this;

    function play() {
      // Request another frame.
      me.autoId = window.requestAnimationFrame(play);

      // Calc elapsed time since last loop.
      me.now = Date.now();
      me.elapsed = me.now - me.then;

      // If enough time has elapsed, draw the next frame.
      if (me.elapsed > me.fpsInterval) {
        // Get ready for next frame by setting then=now, but...
        // Also, adjust for fpsInterval not being multiple of 16.67.
        me.then = me.now - (me.elapsed % me.fpsInterval);

        me.next();
      }
    }

    // Start the animation as requested.
    if (!me.stop) {
      // Initialize the timer variables.
      // @see http://stackoverflow.com/questions/19764018
      // 1000 / me.fps = 5000ms.
      // Ensures to catch the CSS animationend event.
      me.fpsInterval = (me.options.slideSpeed + 1000) || (1000 / (me.options.fps || .2));
      me.then = Date.now();

      play();
    }
  };

  /**
   * Reset the autoplay timer on click, or mouseout.
   */
  _l.autoReset = function () {
    var me = this;

    me.now = Date.now();
    me.elapsed = me.now - me.then;

    me.then = me.now - (me.elapsed % me.fpsInterval);
  };

  /**
   * Cancel the autoplay animation.
   */
  _l.autoCancel = function () {
    var me = this;

    if (me.autoId !== null) {
      window.cancelAnimationFrame(me.autoId);
      me.$slider.classList.add('is-animated', 'is-interrupted');
      me.$slider.classList.remove('is-animating', 'is-slicing');
    }
  };

  /**
   * Reactions based on events to either interrupt, or continue the autoplay.
   *
   * The available classes:
   *   .is-paused is when the slide contains an active/ playing video.
   *   .is-interrupted is to indicate animation is being canceled/ interrupted.
   *
   * @param {Object.Event} e
   *   The event triggered by various `custom` events.
   */
  _l.autoChange = function (e) {
    var me = this;
    var t = e.type;

    me.stop = document[me.hidden] ||
      t === 'mouseenter' ||
      t === 'on.lory.touchstart' ||
      t === 'click' ||
      me.$slider.classList.contains('is-paused');

    if (me.stop) {
      me.autoCancel();
    }
    else {
      me.autoReset();
      me.autoPlay();
    }
  };

}(dBlazy));
