/**
 * @file
 * Provides multi-breakpoint responsive slides.
 */

(function (_db) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Overrides lory.prototype.attachEvents() to add responsive feature.
   */
  _l.attachEvents = (function (_lory) {
    return function () {
      var me = this;

      if (me.options.responsive !== null) {
        var events = {
          'after.lory.init': me.setResponsive,
          'on.lory.resize': me.setResponsive
        };

        // Add event listeners.
        _db.forEach(events, function (fn, event) {
          me.$slider.addEventListener(event, fn.bind(me), false);
        });
      }

      return _lory.call(this);
    };
  })(_l.attachEvents);

  /**
   * Sets current responsive class to the .lory__track container.
   */
  _l.setResponsive = function () {
    var me = this;
    var val = me.getResponsive();

    if (val !== null && me.options.infinite !== val) {
      me.infinite = me.options.infinite = val;
      me.$track.className = me.$track.className.replace(/is-grid--\S+/, 'is-grid--' + val);
    }
  };

  /**
   * Extracts responsive value, and map it to expected infinite value.
   *
   * @return {int|null}
   *  The current window viewport, else null.
   */
  _l.getResponsive = function () {
    var me = this;
    var val = null;
    var resp = me.options.responsive;
    var keys = Object.keys(resp);
    var xs = keys[0];
    var xl = keys[keys.length - 1];

    for (var w in resp) {
      if (resp.hasOwnProperty(w)) {
        if (me.winWidth > w) {
          val = resp[w];
        }
      }
    }

    if (val === null) {
      val = resp[xl <= me.winWidth ? xl : xs];
    }

    return val;
  };

}(dBlazy));
