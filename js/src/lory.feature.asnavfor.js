/**
 * @file
 */

(function (_db) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Overrides lory.prototype.init() to add asNavFor feature.
   */
  _l.afterInit = (function (_lory) {
    return function (e) {
      var me = this;
      var ret = _lory.apply(this, arguments);

      if (me.options.asNavFor) {
        me.$main = me.$wrap.querySelector(me.options.asNavFor + ':not(.lory--nav)');
        me.$nav = me.$wrap.querySelector(me.options.asNavFor + ':not(.lory--main)');

        // From within nav.
        if (me.$main !== null) {
          me.$main.addEventListener('before.lory.slide', me.updateNav.bind(this));
        }

        // From within main: bind nav items as triggers to change main displays.
        if (me.$nav !== null) {
          _db.on(me.$nav, 'click', '.slide', me.focusOnSelect.bind(me));

          // @todo: Remove when main and nav are synced correctly.
          window.setTimeout(function () {
            me.slideTo(me.currentSlide);
          }, 0);
        }
      }

      return ret;
    };
  })(_l.afterInit);

  /**
   * From within nav: update nav states when main display slides.
   *
   * @param {Object.Event} e
   *   The event triggered by various `custom` events.
   */
  _l.updateNav = function (e) {
    var me = this;
    var curr = e.detail.nextSlide !== 'undefined' ? (e.detail.nextSlide - 1) : 0;

    // The asNavFor triggers changing main display.
    if (curr !== 'undefined') {
      // @todo: Recheck when indices corrected.
      // If main display has infinite false, use e.detail.nextSlide.
      // If main display has infinite 1, use e.detail.nextSlide - 1.
      me.slideTo(curr);
    }
  };

}(dBlazy));
