/**
 * @file
 *
 * @todo: Remove when core library supports centerMode.
 */

(function (_db) {

  'use strict';

  // Cache the prototype once.
  var _l = lory.prototype;

  /**
   * Overrides lory.prototype.update() to add centerMode feature.
   */
  _l.update = (function (_lory) {
    return function (e) {
      var me = this;
      var o = me.options;
      var targetLeft = 0;
      var ret = _lory.apply(this, arguments);

      if (!o.infinite || !o.centerMode || o.infinite === 1) {
        return ret;
      }

      if (o.useTransform && !o.vertical) {
        var $curr = me.$clones[me.currentSlide];

        // Only correct multiple grid.
        if ($curr !== null && _db.matches($curr, me.currSel)) {
          o.slidesToScroll = 1;

          targetLeft = $curr.offsetLeft * -1;
          targetLeft += (me.$list.offsetWidth - $curr.offsetWidth) / 2;

          if (e.type === 'after.lory.init') {
            window.setTimeout(function () {
              me.setCss(targetLeft);
            }, 600);
          }
          else {
            me.setCss(targetLeft);
          }
        }
      }

      return _lory.apply(this, arguments);
    };
  })(_l.update);

}(dBlazy));
