
# INTRODUCTION
Touch enabled minimalistic slider written in vanilla JavaScript.

## REQUIREMENTS
* Lory library:
  + Download Lory archive from https://github.com/meandmax/lory
  + Extract it as is, rename *lory-master* to *lory*, so the assets are at:

    `/libraries/lory/dist/lory.min.js`

* Blazy (2.x-rc7+), for DRY stuffs, and as a bonus, advanced lazyloading
  such as delay lazyloading for below-fold sliders, iframe, (fullscreen) CSS
  background lazyloading, breakpoint dependent multi-serving images, lazyload
  ahead for smoother UX.

## OPTIONAL REQUIREMENTS
* Drupal 8.7 drops supporting IE9. If you need to support old browsers (IE9),
  include requestAnimationFrame polyfill into your own custom libraries from any
  below:
  + https://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
  + https://gist.github.com/paulirish/1579671

  Note: IE10+ supports rAF, no need to include it.

* Specific to `arrowDown` feature, it is only enabled if `jumper` module exists.


## INSTALLATION
Install the module as usual:
  Visit `/admin/modules`, and enable the Lory module.

More info can be found on:
  [Installing Drupal 8 Modules](https://drupal.org/node/1897420)


## FEATURES
* A very simple responsive/ multi-breakpoint carousel.
* Touch enabled minimalistic slider written in vanilla JavaScript.
* Hardware accelerated transitions.
* Options for custom easing effects.
* Infinite looping ~ carousel.
* Skins for custom advanced stylings.
* Modular and easily extensible features: asnavfor, autoplay, centermode, dots.
* A few simple CSS3 extensible transitions: fade, Ken Burns, Slicebox 3d --
  converted into vanilla JS, uses Web Animations API, with rAF support.
  None of features and transitions is loaded unless configured so.
* A few Blazy goodness for free: lazy load, lightbox and multimedia integration.


## SAMPLES
The optional samples are provided via Views blocks.
These fields must exist before enabling the module:

* field_image
* field_images

Create a new content type, e.g.: "Lory", or use the existing type, and add 2
image fields with the above-mentioned machine names, then enable the module.
If you have Slick Example in place, those fields are already created, so no
further need to re-create them.


## KNOWN ISSUES
* Lory is only useful when size matters. You have to do your own custom needs
  for those which are not given by default as the author refuses to bake more
  features in. If you need a more robust solution OOTB, check out Slick.

* Indices with focusOnSelect are not correct, yet. Apply the patch to fix it:
  + https://github.com/meandmax/lory/issues/520
  + https://github.com/meandmax/lory/pull/525

* Most module features and skins are experimental.
  A combination of options may break them. Use them at your own risk.
  Use the provided samples to begin width.

  Improvements are very much welcome. Thanks.


## TIPS
* Use the provided hooks, preprocess, or Twig to override/ customize anything.
* All skins are starters, never claimed final, nor perfect. Nobody knows your
  designs better than you, the sample skins are to explore possibility, not to
  dictate.
* Most styling are CSS driven, so it can be easily overriden without touching
  JS, nor extra needs for more configurations.
* To add custom skins, and effect, copy `src/Plugin/lory/LoryAsset`, and adjust
  everything, rename the file, ID, class name, skin definitions, etc. Two
  supported methods: `::setSkins()` and `::setFeatures()`.
  See `Drupal\lory\Plugin\lory\LoryAsset` for more samples.


## CURRENT DEVELOPMENT STATUS
A full release should be reasonable after proper feedback from the community,
some code cleanup, and optimization where needed. Patches are very much welcome.

Alpha and Beta releases are for developers only. Be aware of possible breakage.

However if it is broken, unless an update is explicitly required, clearing cache
should fix most issues during DEV phases.
* Prior to any update, always visit:

  `/admin/config/development/performance`

* And hit "Clear all caches" button once the new Slick is in place.
* Regenerate CSS and JS as the latest fixes may contain changes to the assets.

Have the latest or similar release Blazy to avoid trouble in the first place.


## MAINTAINERS/CREDITS
* [Gaus Surahman](https://drupal.org/user/159062)
* [Contributors](https://www.drupal.org/node/2841533/committers)
* CHANGELOG.txt for helpful souls with their patches, suggestions and reports.

### Slicebox:
* https://github.com/codrops/slicebox
* http://tympanus.net/codrops/2011/09/05/slicebox-3d-image-slider/

Converted into vanilla JS, Web Animations API, with rAF support, by @gausarts.

Some codes are taken from Slick module, and its library.

READ MORE
See the project page on drupal.org: http://drupal.org/project/lory.

More info relevant to each option is available at their form display by hovering
over them, and click a dark question mark.

See the Lory docs at:

* https://github.com/meandmax/lory
* http://meandmax.github.io/lory/
